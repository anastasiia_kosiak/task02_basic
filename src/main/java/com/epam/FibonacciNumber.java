/**
 * package com.epam
 */
package com.epam;
import java.util.Scanner;
/**
 * A class to work with Fibonacci numbers
 */
public class FibonacciNumber {
    public static void main(String[] args){
                int upperBound;
                System.out.println("Enter the upper bound of the sequence");
                Scanner userInput = new Scanner(System.in);
                upperBound = userInput.nextInt();
                printFibonacci(upperBound);
            }
    /**
     * Prints a sequence of Fibonacci numbers and
     * calculates the percentage of odd and even numbers
     * @param max the upper bound of the sequence
     */
    public static void printFibonacci(int max){
        int[] fibonacciSequence;
        fibonacciSequence = new int[max];
        int number1=0;
        int number2=1;
        fibonacciSequence[0]=number1;
        fibonacciSequence[1]=number2;
        int number3;
        int oddPercentage=0;
        int evenPercentage=0;
        int oddCount = 1;
        int evenCount = 1;
        System.out.print(number1+" "+number2);
        for(int i=2;i<fibonacciSequence.length;++i) {
            number3=number1+number2;
            fibonacciSequence[i]=number3;
            if(Number.isEven(fibonacciSequence[i])){
                evenCount++;
            }
            else {
                oddCount++;
            }
            System.out.print(" "+number3);
            number1=number2;
            number2=number3;
        }
        System.out.println();
        oddPercentage=100*oddCount/fibonacciSequence.length;
        evenPercentage=100*evenCount/fibonacciSequence.length;
        System.out.println("The percentage of even numbers: "+ evenPercentage+ "%");
        System.out.println("The percentage of odd numbers "+ oddPercentage +"%");
    }
}
