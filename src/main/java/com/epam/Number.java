/**
 * package com.epam
 */
package com.epam;
import java.util.Scanner;
/**
 * A class to work with numbers
 */
public class Number {
    public static void main(String [] args){
                int lowerBound;
                int upperBound;
                int fibonacciCount;
                Scanner userInput = new Scanner(System.in);
                System.out.println("Enter the interval");
                lowerBound = userInput.nextInt();
                upperBound = userInput.nextInt();
                printNumbers(lowerBound,upperBound);
                printSum(lowerBound,upperBound);
                System.out.println("Enter the upper bound of the Fibonacci sequence");
                fibonacciCount =userInput.nextInt();
                FibonacciNumber.printFibonacci(fibonacciCount);
            }
    /**
     * Checks whether the number is even
     * @param num an integer number
     * @return return a boolean value
     */
            public static boolean isEven(int num){
                return (num%2==0);
            }
    /**
     * Print odd numbers in direct order,
     * and even numbers in reverse order
     * @param min the lower bound of the user interval
     * @param max the upper bound of the user interval
     */
            public static void printNumbers(int min, int max){
                for(int i=min;i<=max;++i) {
                    if (!isEven(i)) {
                        System.out.print(i + " ");
                    }
                }
                System.out.println();
                for(int j=max;j>=min;j--){
                    if(isEven(j)){
                        System.out.print(j+" ");
                    }
                }
                System.out.println();
            }
    /**
     * Prints the sum of all even numbers and
     * the sum of all odd numbers in the interval
     * @param min the lower bound of the user interval
     * @param max the upper bound of the user interval
     */
            public static void  printSum(int min, int max){
                int oddSum=0;
                int evenSum=0;
                for(int i =min;i<=max;++i){
                    if(isEven(i)){
                        evenSum+=i;
                    }
                    else {
                        oddSum+=i;
                    }
                }
                System.out.println("The sum of odd numbers: "+oddSum);
                System.out.println("The sum of even numbers: "+ evenSum);
            }
        }

